import React, { Component } from 'react'
import mapboxgl from 'mapbox-gl';
import config from './env/config'
import {CalPopup, BenPopup} from './Popup'
import ReactDOM from 'react-dom';


class CalContainer extends Component {
    constructor(props) {
        super(props);
        mapboxgl.accessToken = config.accessToken;
        this.state = {
            ...props,
            lng: 5,
            lat: 19,
            zoom: 2
        };
    }
    
    componentDidMount() {
        const map = new mapboxgl.Map({
            container: this.mapContainer,
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [this.state.lng, this.state.lat],
            zoom: this.state.zoom,
            maxBounds: new mapboxgl.LngLatBounds(
                new mapboxgl.LngLat(this.state.sw[0], this.state.sw[1]),
                new mapboxgl.LngLat(this.state.ne[0], this.state.ne[1])
            ),
        });
        map.on('click', (e) => {
            const placeholder = document.createElement('div');
            ReactDOM.render(<CalPopup lng={this.state.lng} lat={this.state.lat} area={this.state.area}/>, placeholder);
            const wrapped = e.lngLat.wrap();
            this.setState({
                lng: wrapped.lng,
                lat: wrapped.lat,
                zoom: map.getZoom().toFixed(2)
            });
            new mapboxgl.Popup()
                .setLngLat([this.state.lng, this.state.lat])
                .setDOMContent(placeholder)
                .addTo(map)
        });
    }

    render() {
        return (
            <div>
                <div ref={el => this.mapContainer = el} className="mapContainer" />
            </div>
        )
    }

}

class BenContainer extends Component {
    constructor(props) {
        super(props);
        mapboxgl.accessToken = config.accessToken;
        this.state = {
            ...props,
            lng: 5,
            lat: 19,
            zoom: 2
        };
    }
    
    componentDidMount() {
        const map = new mapboxgl.Map({
            container: this.mapContainer,
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [this.state.lng, this.state.lat],
            zoom: this.state.zoom,
            maxBounds: new mapboxgl.LngLatBounds(
                new mapboxgl.LngLat(this.state.sw[0], this.state.sw[1]),
                new mapboxgl.LngLat(this.state.ne[0], this.state.ne[1])
            ),
        });
        map.on('click', (e) => {
            const placeholder = document.createElement('div');
            const wrapped = e.lngLat.wrap();
            this.setState({
                lng: wrapped.lng,
                lat: wrapped.lat,
                zoom: map.getZoom().toFixed(2)
            });
            ReactDOM.render(<BenPopup lng={this.state.lng} lat={this.state.lat} area={this.state.area} />, placeholder);
            new mapboxgl.Popup()
                .setLngLat([this.state.lng, this.state.lat])
                .setDOMContent(placeholder)
                .addTo(map)
        });
    }

    render() {
        return (
            <div>
                <div ref={el => this.mapContainer = el} className="mapContainer" />
            </div>
        )
    }

}

export  {CalContainer, BenContainer};
