import React, { Component } from 'react'
import * as tf from '@tensorflow/tfjs';



class CalPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            prediction: 0,
            correct: true
        }
        this.age = 0;
        this.rooms = 0;
        this.bedrooms = 0;
        this.population = 0;
        this.households = 0;
        this.income = 0;
        this.loadModel = this.loadModel.bind(this);
        this.err = [];
    }

    async loadModel() {
        const model = await tf.loadLayersModel('http://localhost:3000/predict1/model.json');
        this.setState({
            prediction: model.predict(tf.tensor([[this.props.lng, this.props.lat, this.age, this.rooms, this.bedrooms, this.population, this.households, this.income / 10000]])).dataSync(),
            correct: true
        })
    }

    render() {
        return (
            <div>
                <h4 id="pop-head">Predict House Price</h4>
                <div className="holder">
                    <div className='info'>All data must be entered for 1425.5 individuals in a geographically compact area</div>
                    <div className="entitiy">
                        <label>Age: <input className="in" type="number" onKeyUp={(e) => { this.age = Number(e.target.value) }} /></label>
                    </div>
                    <div className="entitiy">
                        <label>Rooms: <input className="in" type="number" onKeyUp={(e) => { this.rooms = Number(e.target.value) }} /></label>
                    </div>
                    <div className="entitiy">
                        <label>Bedrooms <input className="in" type="number" onKeyUp={(e) => { this.bedrooms = Number(e.target.value) }} /></label>
                    </div>
                    <div className="entitiy">
                        <label>Population <input className="in" type="number" onKeyUp={(e) => { this.population = Number(e.target.value) }} /></label>
                    </div>
                    <div className="entitiy">
                        <label>households <input className="in" type="number" onKeyUp={(e) => { this.households = Number(e.target.value) }} /></label>
                    </div>
                    <div className="entitiy">
                        <label>income <input className="in" type="number" onKeyUp={(e) => { this.income = Number(e.target.value) }} /></label>
                    </div>
                    <div className="submit">
                        <input type="submit" value="Submit" onClick={() => {
                            if (this.age < 10 || this.age > 100) {
                                this.err.push('Age should be greater than 10 and less than 100')
                            }
                            if (this.rooms < 100 || this.rooms > 3000) {
                                this.err.push(' Number of rooms should be greater than 100 and less than 3000')
                            }
                            if (this.bedrooms < 100 || this.bedrooms > 3000) {
                                this.err.push(' Number of bedrooms should be greater than 100 and less than 3000')
                            }
                            if (this.population < 100 || this.population > 6000) {
                                this.err.push(' Number of population should be greater than 100 and less than 6000')
                            }
                            if (this.households < 100 || this.households > 1200) {
                                this.err.push(' Number of households should be greater than 100 and less than 1200')
                            }
                            if (this.income < 8000 || this.income > 100000) {
                                this.err.push(' Median income should be greater than 8000 and less that 100000 dollars')
                            }
                            
                            if (this.err.length > 0) {
                                this.setState({ correct: false })
                            } else {
                                this.loadModel();
                            }
                        }} />
                    </div>
                </div>
                <div>
                    {this.state.correct ? <div>Predicted Price: {this.state.prediction} &#36;</div> : <div className='err'>Incorrect Values entered. {this.err.join()}</div>}
                </div>
            </div>
        );
    }
}

class BenPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            prediction: 0,
            correct: true
        }
        this.area = 0;
        this.rooms = 1;
        this.furnished = 0;
    }

    async loadModel() {
        const model = await tf.loadLayersModel('http://localhost:3000/predictb/model.json');

        const dist = this.latlngToDist(this.props.lat, this.props.lng)
        console.log(this.props.lng, this.props.lat)
        console.log(this.area, this.rooms, dist, this.furnished)
        this.setState({
            prediction: model.predict(tf.tensor([[this.area, this.rooms, dist, this.furnished]])).dataSync(),
            correct: true
        })
    }

    latlngToDist(lat, lng) {
        let r = 6373;
        let latC = (12.972442 * Math.PI) / 180;
        let lngC = (77.580643 * Math.PI) / 180;
        let dLat = latC - ((lat * Math.PI) / 180);
        let dLng = lngC - ((lng * Math.PI) / 180);
        let a = ((Math.sin(dLat / 2) ** 2) + (Math.cos(lat) * Math.cos(latC) * (Math.sin(dLng / 2) ** 2)));
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(Math.abs(1 - a)));
        return (r * c);
    }

    render() {
        return (
            <div>
                <h4 id="pop-head">Predict House Price</h4>
                <div className="holder">
                    <div className="entitiy">
                        <label>Area in sq feet<input className="in" min="200" max="6000" type="number" onKeyUp={(e) => { this.area = Number(e.target.value) }} /></label>
                    </div>
                    <div className="entitiy">
                        <label>Number of bedrooms:</label>
                        <select id="rooms" onChange={(e) => { this.rooms = Number(e.target.value); console.log(this.rooms) }}>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                        </select>
                    </div>
                    <div className="entitiy">
                        <label>Unfurnished <input className="in" type="checkbox" onChange={(e) => { this.furnished = Number(e.target.checked) }} /></label>
                    </div>
                    <div className="submit">
                        <input type="submit" value="Submit" onClick={() => {
                            if (this.area > 6000 || this.area < 200) {
                                this.setState({ 'correct': false });
                            } else {
                                this.loadModel();
                            }
                        }} />
                    </div>
                </div>
                {this.state.correct ? <div>Predicted Price: {this.state.prediction} &#8377;</div> : <div className='err'>{'Invalid area entered. Area must be between 200 to 6000 sq ft'}</div>}
            </div>
        );
    }
}

export { CalPopup, BenPopup };