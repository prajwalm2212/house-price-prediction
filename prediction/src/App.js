import React, { Component } from 'react';
import './App.css';
import { CalContainer, BenContainer } from "./MapContainer";
import '../src/style.css'


class App extends Component {

    constructor(props) {
        super(props)
        this.state = {
            area: 'cal',
            sw: [-126.452041, 29.850057],
            ne: [-115.806289, 41.951220]
        }
    }

    // sw -> 77.406817, 12.789188
    // ne -> 77.817697, 13.188728

    render() {
        console.log('Upadted')
        return (
            <div>
                <div className='nav'>
                    <h1 className="title">House Price Prediction</h1>
                    <button className='btn' onClick={() => this.setState({
                        area: 'cal',
                        sw: [-126.452041, 29.850057],
                        ne: [-115.806289, 41.951220]
                    })}>California</button>
                    <button className='btn' onClick={() => this.setState({
                        area: 'ben',
                        sw: [77.406817, 12.789188],
                        ne: [77.817697, 13.188728]
                    })}>Bengaluru</button>
                </div>

                {
                    this.state.area === 'cal' ?
                        <CalContainer place={this.state.area}
                            sw={this.state.sw}
                            ne={this.state.ne} />
                        :
                        <BenContainer place={this.state.area}
                            sw={this.state.sw}
                            ne={this.state.ne} />
                }
            </div>
        )
    }
}

export default App;
